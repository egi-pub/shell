module gitlab.com/egi-pub/shell

go 1.13

require (
	github.com/abiosoft/ishell v2.0.0+incompatible
	github.com/abiosoft/readline v0.0.0-20180607040430-155bce2042db // indirect
	github.com/fatih/color v1.9.0
	github.com/flynn-archive/go-shlex v0.0.0-20150515145356-3f9db97f8568
	github.com/stretchr/testify v1.5.1
	gitlab.com/egi-pub/readline v0.0.0-20200223211052-d9ec76bdadb8
)
