// +build windows

package ishell

import (
	"gitlab.com/egi-pub/readline"
)

func clearScreen(s *Shell) error {
	return readline.ClearScreen(s.writer)
}
